<?php

class CategorySeed extends Seeder
{


    public function run()
    {

        DB::table('categories')->delete();

        $categories = [];
        $categories[]['name'] = 'Smieszne';
        $categories[]['name'] = 'Prawdziwe';
        $categories[]['name'] = 'Z życia wzięte';
        $categories[]['name'] = 'Erotyczne';
        $categories[]['name'] = 'Sex';
        $categories[]['name'] = 'Z pracy';
        $categories[]['name'] = 'Informatyk';
        $categories[]['name'] = 'Rodzina';
        $categories[]['name'] = 'Babcia';
        $categories[]['name'] = 'Dziadek';
        $categories[]['name'] = 'Lubię placki';
        $categories[]['name'] = 'Na gorąco';
        $categories[]['name'] = 'Telefony';
        $categories[]['name'] = 'Sklep';
        $categories[]['name'] = 'Plotki';
        $categories[]['name'] = 'Sport';
        $categories[]['name'] = 'TVN';
        $categories[]['name'] = 'Polsat';
        $categories[]['name'] = 'TVP';
        $categories[]['name'] = 'Celebryci';
        $categories[]['name'] = 'Gwiazdy';
        $categories[]['name'] = 'Showbuisnes';
        $categories[]['name'] = 'Kot';
        $categories[]['name'] = 'Pies';
        $categories[]['name'] = 'Horror';
        $categories[]['name'] = 'Straszne';
        $categories[]['name'] = 'Przydatne';
        $categories[]['name'] = 'Na co dzień';
        $categories[]['name'] = 'Suchary';
        $categories[]['name'] = 'Polska';

        foreach ($categories as &$name) {
            $name['slug'] = Str::slug($name['name']);
        }

        Category::insert($categories);

        $this->command->info('categories table seeded!');
    }

}