<?php


/**
 * User
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $email
 * @property string $first_name
 * @property string $gender
 * @property string $last_name
 * @property string $link
 * @property string $locale
 * @property string $name
 * @property string $accessToken
 * @property string $timezone
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAccessToken($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereTimezone($value)
 * @property integer $facebook_id
 * @property integer $age_range_max
 * @property integer $age_range_min
 * @property string $ip
 * @property string $nickname
 * @property-read \Illuminate\Database\Eloquent\Collection|\Story[] $story
 * @property-read \Illuminate\Database\Eloquent\Collection|\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\UserHistory[] $history
 * @method static \Illuminate\Database\Query\Builder|\User whereFacebookId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAgeRangeMax($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAgeRangeMin($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereNickname($value)
 * @property string $avatar
 * @method static \Illuminate\Database\Query\Builder|\User whereAvatar($value)
 */
class User extends Eloquent
{

    public $hidden = [
        'facebook_id',
        'email',
        'first_name',
        'gender',
        'last_name',
        'link',
        'locale',
        'name',
        'accessToken',
        'ip',
        'age_range_max',
        'age_range_min'
    ];

    protected $fillable = [
        'facebook_id',
        'email',
        'first_name',
        'gender',
        'last_name',
        'link',
        'locale',
        'name',
        'accessToken',
        'timezone',
        'nickname',
        'nickname_slug',
        'avatar',
        'ip',
        'age_range_max',
        'age_range_min',
        'load_comments',
        'allow_notification'
    ];

    public function story()
    {
        return $this->hasMany('Story');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function history()
    {
        return $this->hasMany('UserHistory');
    }


}
