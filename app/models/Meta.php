<?php


/**
 * Meta
 *
 * @property integer $user_id
 * @property integer $story_id
 * @property boolean $read
 * @property boolean $favorite
 * @property-read \Story $story
 * @method static \Illuminate\Database\Query\Builder|\Meta whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Meta whereStoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Meta whereRead($value)
 * @method static \Illuminate\Database\Query\Builder|\Meta whereFavorite($value)
 */
class Meta extends Eloquent
{

    protected $table = 'meta';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'story_id',
        'read',
        'favorite'
    ];

    public function story()
    {
        return $this->belongsTo('Story');
    }


}
