<?php


/**
 * CustomSession
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $token
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereToken($value)
 * @property string $platform
 * @method static \Illuminate\Database\Query\Builder|\CustomSession wherePlatform($value)
 */
class CustomSession extends Eloquent
{
    protected $table = 'sessions';
    
    protected $fillable = [
        'id',
        'token',
        'platform'
    ];


}
