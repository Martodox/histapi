<?php

/**
 * Category
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\Story[] $stories
 * @method static \Illuminate\Database\Query\Builder|\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereSlug($value)
 */
class Category extends Eloquent
{
    protected $table = 'categories';
    public $timestamps = false;

    protected $fillable = [
        'tag',
        'slug'
    ];

    public function stories()
    {
        return $this->belongsToMany('Story')->withPivot('category_id', 'story_id');
    }

}
