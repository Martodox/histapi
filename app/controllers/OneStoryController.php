<?php

class OneStoryController extends Controller
{

    public function getOne($id)
    {
        $errors = [];

        try {
            $one = Story::where('id', $id)->with(['user' => function ($q) {
                $q->select('id', 'nickname', 'nickname_slug', 'avatar');
            }])->with('category');


            $user = UserHelp::getUser(true);

            if ($user) {
                $one = $one->with(['meta' => function ($q) use ($user) {
                    $q->where('user_id', $user->id)->
                    select('story_id', 'read', 'favorite');
                }]);
            }


            $one = $one->limit(1)->get();

            $filter = $one->first();

            $continue = true;

            if ($filter->active == 0) {
                if (!$user) {
                    $continue = false;
                } else {
                    if ($user->id != $filter->user->id) {
                        $continue = false;
                    }
                }
            }

            if ($continue) {

                $one = \StoryController::mapResponse($one, $user);
                $one = $one->first();

            } else {
                $one = [];
            }


        } catch (Exception $e) {
            $errors[] = 'story not found';
        }

        if (empty($one)) {
            $one = new stdClass();
            $errors[] = 'story not found';
        }


        return Response::json(['story' => $one, 'errors' => $errors], 200);
    }

    public function getComments($id)
    {
        try {
            $one = Comment::where('story_id', $id)->with(['user' => function ($q) {
                $q->select('id', 'nickname', 'nickname_slug', 'avatar');
            }])->orderBy('created_at', 'desc');
        } catch (Exception $e) {
            $one = $e;
        }

        $response = $one->get()->toArray();

        $nestedList = [];
        $replies = [];

        foreach ($response as $comment) {
            $comment['user']['avatar'] = UserHelp::smallAvatar($comment['user']['avatar']);
            if ($comment['parent_id'] > 0) {
                $replies[] = $comment;
            } else {
                $comment['replies'] = [];
                $nestedList[$comment['id']] = $comment;
            }
        }

        foreach ($replies as $reply) {
            $nestedList[$reply['parent_id']]['replies'][] = $reply;
        }


        return Response::json(['comments' => array_values($nestedList)], 200);
    }

    public function addComment($id)
    {
        $success = [];
        $errors = [];

        $user = UserHelp::getUser();

        $input = [];

        $input['id'] = $id;
        $input['parent_id'] = Sanitize::get('parent_id', 0);
        $input['content'] = Sanitize::get('content');


        //TODO: validate!
        $comment = new Comment();


        $comment->story_id = $id;
        $comment->content = trim($input['content']);
        $comment->user_id = $user->id;
        $comment->parent_id = $input['parent_id'];
        $comment->active = 1;
        $comment->save();
        $comment_id = $comment->id;

        Story::find($id)->increment('comment_count');

        $success[] = 'Komentarz został dodany.';

        return Response::json(['status' => true, 'comment_id' => $comment_id, 'success' => $success, 'errors' => $errors], 200);

    }

    public function addNew()
    {

        $user = UserHelp::getUser();

        $input = [];
        $input['content'] = Sanitize::get('content');
        $input['category'] = Sanitize::get('category');
        $input['title'] = Sanitize::get('title');

        //TODO: validate, use akimset

        $valid = true;
        $errors = [];
        $success = [];
        if (empty($input['category'])) {
            $valid = false;
            $errors[] = 'Historia musi być przypisana do jednej kategorii.';
            $storyId = -1;
        }

        if ($valid) {
            $story = new Story();
            $story->content = $input['content'];
            $story->active = false;
            $story->user_id = $user->id;
            $story->language = 'pl';
            $story->title = mb_strtoupper($input['title']);
            $story->save();
            $story->category()->attach([$input['category']]);
            $storyId = $story->id;
            $success[] = 'Historia nr ' . $storyId . ' została zapisana';
        }

        return Response::json(['status' => $valid, 'id' => $storyId, 'errors' => $errors, 'success' => $success], 200);
    }

    public function editStory()
    {
        $user = UserHelp::getUser();
        $input = [];

        $errors = [];
        $success = [];
        $input['content'] = Sanitize::get('content');
        $input['id'] = Sanitize::get('id');
        $input['title'] = Sanitize::get('title');


        $story = Story::find($input['id']);

        if ($story->user_id == $user->id && $story->active == 0) {
            $story->content = $input['content'];
            $story->title = $input['title'];
            $story->save();
            $success[] = 'Zmiany zostały zapisane.';
        } else {
            $errors[] = 'Nie posiadasz uprawnień do edycji';
        }

        return Response::json(['story' => $story, 'errors' => $errors, 'success' => $success], 200);

    }

    public function abuse($id)
    {

        $errors = [];
        $success = [];

        $user = UserHelp::getUser(true);

        $reason = Sanitize::get('reason');

        $abuse = new Abuse();

        $abuse->story_id = $id;
        $abuse->reason = $reason;
        if ($user) {
            $abuse->user_id = $user->id;
        }

        $abuse->save();

        $success[] = 'Zgłoszenie nr ' . $abuse->id . ' zostało przyjęte.';


        return Response::json(['success' => $success, 'errors' => $errors], 200);


    }


}
