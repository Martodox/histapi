<?php


class Abuse extends Eloquent
{


    protected $table = 'abuse';

    protected $fillable = [
        'reason',
        'user_id',
        'story_id',
        'resolved'
    ];

    public function story()
    {
        return $this->belongsTo('Story');
    }


}
