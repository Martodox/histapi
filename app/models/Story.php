<?php


/**
 * Story
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $content
 * @property string $title
 * @property integer $user_id
 * @property boolean $active
 * @property string $language
 * @method static \Illuminate\Database\Query\Builder|\Story whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereLanguage($value)
 * @property integer $category_id
 * @property integer $favorite_count
 * @property integer $read_count
 * @property integer $comment_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Meta[] $meta
 * @property-read \User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Tag[] $tags
 * @property-read \Category $category
 * @method static \Illuminate\Database\Query\Builder|\Story whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereFavoriteCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereReadCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereCommentCount($value)
 */
class Story extends Eloquent
{
    protected $hidden = [
        'user_id',
        'updated_at',
        'read_count'
    ];

    protected $fillable = [
        'content',
        'active',
        'language',
        'title',
        'user_id',
        'favorite_count',
        'comment_count',
        'read_count'
    ];

    public function meta()
    {
        return $this->hasMany('Meta');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function reports()
    {
        return $this->hasMany('Abuse');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag')->withPivot('story_id', 'tag_id');
    }

    public function category()
    {
        return $this->belongsToMany('Category')->withPivot('story_id', 'category_id');;
    }


}
