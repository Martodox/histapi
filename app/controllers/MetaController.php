<?php

class MetaController extends BaseController
{

    public function favorite()
    {
        $user = UserHelp::getUser();
        return $this->mark('favorite', 'read', $user);
    }

    public function read()
    {
        $user = UserHelp::getUser();
        return $this->mark('read', 'favorite', $user);
    }

    private function mark($what, $extra, $user)
    {

        $story = Input::get('id', null);

        $meta = Meta::where('user_id', $user->id)->where('story_id', '=', $story);

        $db = $meta->get()->first();

        $status = ['read' => 0, 'favorite' => 0];

        $decrement = [];
        $increment = [];

        if (empty($db)) {
            $db = new Meta();
            $db->user_id = $user->id;
            $db->story_id = $story;
            $db->$what = true;
            $status[$what] = 1;

            if ($what == 'favorite') {
                $db->read = true;
                $status['read'] = 1;
                $increment[] = 'read';
            }

            $increment[] = $what;

            $db->save();
        } else {

            $status = ['read' => $db['read'], 'favorite' => $db['favorite']];

            if ($what == 'read') {
                $status['read'] = 1 - $status['read'];

                if ($status['read'] == 1) {
                    $increment[] = 'read';
                } else {

                    if ($status['favorite'] == 1) {
                        $status['favorite'] = 0;
                        $decrement[] = 'favorite';
                    }

                    $decrement[] = 'read';
                }
            }


            if ($what == 'favorite') {
                $status['favorite'] = 1 - $status['favorite'];

                if ($status['favorite'] == 1) {
                    $increment[] = 'favorite';

                    if ($status['read'] == 0) {
                        $status['read'] = 1;
                        $increment[] = 'read';
                    }


                } else {
                    $decrement[] = 'favorite';

                    if ($status['read'] == 1) {
                        $status['read'] = 0;
                        $decrement[] = 'read';
                    }
                }
            }

            $meta->update($status);

            if ($status['read'] == 0 && $status['favorite'] == 0) {
                Meta::where('user_id', $user->id)->where('story_id', '=', $story)->delete();
            }

        }


        $this->setMetaCount($increment, $decrement, $story);

        $value = (bool)$status[$what];


        return Response::json(['res' => $value, 'status' => $status], 200);
    }


    private function setMetaCount($increment, $decrement, $id)
    {
        $story = Story::find($id);

        foreach ($increment as $column) {
            $story->increment($column . '_count');
        }

        foreach ($decrement as $column) {
            $story->decrement($column . '_count');
        }

    }

}
