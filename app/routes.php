<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials: true');

Route::pattern('id', '[0-9]+');
Route::pattern('commentId', '[0-9]+');

Route::filter('authorisation', 'LoginController@checkHeader');


Route::get('/', function () {
    return Response::json(['status' => 'not found'], 404);
});

Route::group(['prefix' => 'api/v1'], function () {

    Route::group(['prefix' => '/content'], function () {

        Route::post('/', [
            'before' => 'authorisation',
            'uses' => 'OneStoryController@addNew'
        ]);

        Route::post('/edit', [
            'before' => 'authorisation',
            'uses' => 'OneStoryController@editStory'
        ]);

        Route::get('/latest/{id?}', [
            'uses' => 'StoryController@getLatest'
        ]);

        Route::get('/favorite/{id?}', [
            'uses' => 'StoryController@getFavorite'
        ]);
        Route::get('/read/{id?}', [
            'before' => 'authorisation',
            'uses' => 'StoryController@getRead'
        ]);

        Route::get('/category/{slug}/{id?}', [
            'uses' => 'StoryController@getByCategory'
        ]);

        Route::group(['prefix' => '/profile/{slug}'], function () {

            Route::get('/', [
                'uses' => 'PublicProfile@user'
            ]);

            Route::get('/posts/{id?}', [
                'uses' => 'PublicProfile@posts'
            ]);

        });

    });


    Route::group(['before' => 'authorisation', 'prefix' => '/mark'], function () {

        Route::post('/favorite', [
            'uses' => 'MetaController@favorite'
        ]);

        Route::post('/read', [
            'uses' => 'MetaController@read'
        ]);
    });

    Route::group(['prefix' => '/login'], function () {

        Route::post('/facebook', [
            'uses' => 'LoginController@authenticate'
        ]);

        Route::post('/logout', [
            'before' => 'authorisation',
            'uses' => 'LoginController@logout'
        ]);

    });

    Route::group(['prefix' => '/story/{id}'], function () {

        Route::get('/', [
            'uses' => 'OneStoryController@getOne'
        ]);

        Route::get('/comments/', [
            'uses' => 'OneStoryController@getComments'
        ]);

        Route::post('/comments', [
            'before' => 'authorisation',
            'uses' => 'OneStoryController@addComment'
        ]);

        Route::post('/report', [
            'uses' => 'OneStoryController@abuse'
        ]);


    });

    Route::group(['prefix' => '/tags'], function () {
        Route::get('/get/all', [
            'uses' => 'TagController@all'
        ]);

        Route::get('/{part}', [
            'uses' => 'TagController@listTags'
        ]);

    });

    Route::group(['prefix' => '/categories'], function () {

        Route::get('/', [
            'uses' => 'CategoryController@all'
        ]);

    });

    Route::group(['before' => 'authorisation', 'prefix' => '/profile'], function () {

        Route::get('/', [
            'uses' => 'UserController@getProfile'
        ]);

        Route::post('/update', [
            'uses' => 'UserController@updateProfile'
        ]);

    });

//    Route::group(['prefix' => '/moderate'], function () {
//
//        Route::get('/queue', [
//            'uses' => 'ModerateController@all'
//        ]);
//
//        Route::get('/facebook/{id}', [
//           'uses' => 'ModerateController@postToFacebook'
//        ]);
//
//    });


});


App::missing(function ($exception) {
    return Redirect::to('/');
});