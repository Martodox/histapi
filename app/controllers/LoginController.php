<?php

class LoginController extends BaseController
{

    private $headerValidity = 86400;

    public function authenticate()
    {

        $token = true;
        $facebook_user = [];

        $accessToken = Input::get('accessToken', null);
        $facebookID = Input::get('facebookID', null);
        $platform = Input::get('platform', null);

        try {
            Facebook::setAccessToken($accessToken);
            $facebook_user =
                Facebook::object('me')->
                fields('email', 'first_name', 'gender', 'last_name', 'link', 'locale', 'name', 'timezone', 'age_range')->
                get();
        } catch (Exception $e) {
            $token = false;
        }

        if ($token) {
            if ($facebookID == $facebook_user['id']) {
                $token = $this->updateSession($facebook_user['id'], $platform);
                $facebook_user = json_decode($facebook_user, true);
                $facebook_user = $this->updateUser($facebook_user, $accessToken, $platform);
            } else {
                $token = false;
            }
        }

        return Response::json(['authToken' => $token, 'user' => $facebook_user], 200);

    }


    public function logout()
    {
        $user = UserController::getUser();
        CustomSession::destroy($user['id']);
        return Response::json(['status' => 'ok'], 200);
    }

    public function updateSession($id, $platform)
    {


        if (!in_array($platform, ['www', 'and', 'ios', 'bb', 'wp'])) {
            App::abort(401, 'Unauthorized platform.');
        }

        $session = CustomSession::where('id', $id)->where('platform', $platform);

        $bdSession = $session->get()->first();

        if (empty($bdSession)) {
            $token = $this->getToken();

            $customSession = new CustomSession();

            $customSession->id = $id;
            $customSession->platform = $platform;
            $customSession->token = $token;
            $customSession->save();

        } else {

            $difference = (strtotime(date('Y-m-d H:i:s')) - strtotime($bdSession->updated_at)) / $this->headerValidity;

            if ($difference > 1) {
                $token = $this->getToken();
                $session->update([
                    'id' => $id,
                    'platform' => $platform,
                    'token' => $token
                ]);
            } else {
                $token = $bdSession->token;
            }


        }

        return $token;

    }

    private function getToken()
    {
        return Hash::make(Session::get('_token') . microtime());
    }

    public function updateUser($array, $token, $platform)
    {

        if (!empty($array['age_range']['min'])) {
            $array['age_range_min'] = $array['age_range']['min'];
        }

        if (!empty($array['age_range']['max'])) {
            $array['age_range_max'] = $array['age_range']['max'];
        }

        unset($array['age_range']);

        $array['facebook_id'] = $array['id'];
        unset($array['id']);


        $session = User::where('facebook_id', $array['facebook_id'])->limit(1)->get()->first();

        if (empty($session)) {
            $array['nickname'] = substr($array['first_name'], 0, 3) . substr($array['last_name'], 0, 3) . substr(md5($array['facebook_id']), 0, 5);
            $array['nickname_slug'] = Str::slug($array['nickname']);
            $array['ip'] = Request::getClientIp();
            $array['avatar'] = Request::root() . '/avatars/default.jpg';
            $user = User::create($array);
            $first_login = true;

            $id = $user->id;

        } else {
            $first_login = false;
            $array['nickname'] = $session->nickname;
            $array['nickname_slug'] = $session->nickname_slug;
            $array['avatar'] = $session->avatar;
            $array['load_comments'] = $session->load_comments;
            User::where('facebook_id', $array['facebook_id'])->update($array);
            $id = $session->id;
        }

        $date = UserHistory::where('user_id', $id)->orderBy('created_at', 'desc')->limit(1)->get()->first();

        if (empty($date)) {
            $array['last_login'] = date('Y-m-d H:i:s');
        } else {
            $array['last_login'] = $date->created_at->toDateTimeString();
        }

        $history = new UserHistory();
        $history->user_id = $id;
        $history->ip = Request::getClientIp();
        $history->token = $token;
        $history->platform = $platform;
        $history->save();

        $user = new User();

        foreach ($user->hidden as $key) {
            if (isset($array[$key])) {
                unset($array[$key]);
            }
        }

        $array['avatar_s'] = UserHelp::smallAvatar($array['avatar']);
        $array['first_login'] = $first_login;
        return $array;

    }

    public function checkHeader()
    {
        $authHeader = Request::header('authorization');

        if (empty($authHeader)) {
            return Response::json('not authorised', 401);
        }

    }


}
