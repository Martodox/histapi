<?php

class Sanitize
{

    public static function stripTags($string)
    {
        if (is_array($string)) {
            return self::stripTagsAll($string);
        }
        return strip_tags(trim($string));
    }

    public static function stripTagsAll($array)
    {
        if (empty($array)) {
            return $array;
        }

        $result = array();
        foreach ($array as $key => $value) {
            $key = self::stripTags($key);
            if (is_array($value)) {
                $result[$key] = slef::stripTagsAll($value);
            } else {
                $result[$key] = self::stripTags($value);
            }
        }
        return $result;
    }

    public static function get($key, $default = null)
    {
        return self::stripTags(Input::get($key, $default));
    }
}