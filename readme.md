# Lista endpointów API


```
#!php

Route::group(array('prefix' => 'api/v1'), function () {

    Route::group(array('prefix' => '/content'), function () {
        Route::post('/', array('before' => 'authorisation', 'uses' => 'StoryController@addNew'));
        Route::get('/latest/{id?}', array('uses' => 'StoryController@getLatest'));
        Route::get('/favorite/{id?}', array('before' => 'authorisation', 'uses' => 'StoryController@getFavorite'));
        Route::get('/read/{id?}', array('before' => 'authorisation', 'uses' => 'StoryController@getRead'));
    });


    Route::group(array('before' => 'authorisation', 'prefix' => '/mark'), function () {
        Route::post('/favorite', array('uses' => 'MetaController@favorite'));
        Route::post('/read', array('uses' => 'MetaController@read'));
    });

    Route::group(array('prefix' => '/login'), function () {
        Route::post('/facebook', array('uses' => 'LoginController@authenticate'));
        Route::post('/logout', array('before' => 'authorisation', 'uses' => 'LoginController@logout'));
    });

    Route::group(array('prefix' => '/story'), function () {
        Route::get('/{id}', array('uses' => 'OneStoryController@getOne'));
        Route::get('/{id}/comments/{commentId?}', array('uses' => 'OneStoryController@getComments'));
        Route::post('/{id}/comments', array('before' => 'authorisation', 'uses' => 'OneStoryController@addComment'));

    });

    Route::group(array('prefix' => '/tags'), function () {
        Route::get('/{part}', array('uses' => 'TagController@listTags'));

    });

});
```



