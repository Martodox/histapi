<?php

class PublicProfile extends BaseController
{

    private function getUser($slug)
    {

        $response = User::where('nickname_slug', $slug)->get();

        $erase = ['allow_notification', 'load_comments'];

        $user = $response->map(function ($one) use ($erase) {
            foreach ($erase as $key) {
                unset($one[$key]);
            }
            return $one;
        });

        $user = $user->first();

        return $user;
    }


    public function user($slug)
    {
        $errors = [];

        $user = $this->getUser($slug);

        if ($user == null) {
            $user = false;
            $errors[] = 'Użytkownik nie został znaleziony';
        }

        return Response::json(['user' => $user, 'errors' => $errors], 200);

    }

    public function posts($slug, $id = false)
    {
        $errors = [];

        $user = UserHelp::getUser(true);

        $slugUser = $this->getUser($slug);

        if ($slugUser == null) {
            $new = [];
            $errors[] = 'Użytkownik nie został znaleziony';
        } else {

            $chunkSize = StoryController::$chunkSize;

            $query =
                Story::limit($chunkSize)->
                where('user_id', $slugUser->id)->
                orderBy('id', 'desc')->
                with(['user' => function ($q) {
                    $q->select('id', 'nickname', 'nickname_slug', 'avatar');
                }])->
                with('category');

            if ($user) {
                $query->with(['meta' => function ($q) use ($user) {
                    $q->where('user_id', $user->id)->
                    select('story_id', 'read', 'favorite');
                }]);
            }

            if (!$user || ($user && $slugUser->id != $user->id)) {
                $query->where('active', '=', 1);
            }

            if (!$id) {
                $response = $query->get();
            } else {
                $response = $query->where('id', '<', $id)->get();
            }

            $new = StoryController::mapResponse($response, $user);
        }

        return Response::json(['content' => $new, 'errors' => $errors], 200);

    }

}
