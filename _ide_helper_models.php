<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * User
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $email
 * @property string $first_name
 * @property string $gender
 * @property string $last_name
 * @property string $link
 * @property string $locale
 * @property string $name
 * @property string $accessToken
 * @property string $timezone
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAccessToken($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereTimezone($value)
 * @property integer $index
 * @property string $nickname
 * @property-read \Illuminate\Database\Eloquent\Collection|\Story[] $story
 * @property-read \Illuminate\Database\Eloquent\Collection|\Comment[] $comments
 * @method static \Illuminate\Database\Query\Builder|\User whereIndex($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereNickname($value) 
 */
	class User {}
}

namespace {
/**
 * Meta
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $story_id
 * @property boolean $read
 * @property boolean $favorite
 * @property-read \Story $story
 * @method static \Illuminate\Database\Query\Builder|\Meta whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Meta whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Meta whereStoryId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Meta whereRead($value) 
 * @method static \Illuminate\Database\Query\Builder|\Meta whereFavorite($value) 
 */
	class Meta {}
}

namespace {
/**
 * Comment
 *
 * @property integer $id
 * @property integer $story_id
 * @property integer $user_id
 * @property integer $parent_id
 * @property boolean $active
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $user
 * @method static \Illuminate\Database\Query\Builder|\Comment whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereStoryId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereParentId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereActive($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereContent($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUpdatedAt($value) 
 */
	class Comment {}
}

namespace {
/**
 * Story
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $content
 * @property string $title
 * @property integer $user_id
 * @property boolean $active
 * @property string $language
 * @method static \Illuminate\Database\Query\Builder|\Story whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Story whereLanguage($value)
 * @property integer $favorites
 * @property \Illuminate\Database\Eloquent\Collection|\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Meta[] $meta
 * @property-read \User $user
 * @method static \Illuminate\Database\Query\Builder|\Story whereFavorites($value) 
 * @method static \Illuminate\Database\Query\Builder|\Story whereComments($value) 
 */
	class Story {}
}

namespace {
/**
 * CustomSession
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $token
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomSession whereToken($value)
 */
	class CustomSession {}
}

