<?php

class StoryController extends BaseController
{
    public static $chunkSize = 30;


    public function getLatest($id = false)
    {
        $user = UserHelp::getUser(true);
        return $this->getData($id, 'latest', $user);
    }

    public function getRead($id = false)
    {
        $user = UserHelp::getUser();
        return $this->getData($id, 'read', $user);
    }

    public function getFavorite($id = false)
    {
        $user = UserHelp::getUser();
        return $this->getData($id, 'favorite', $user);
    }

    public function getByTags($id = false)
    {
        $user = UserHelp::getUser(true);
        return $this->getData($id, 'tags', $user);
    }

    public function getByCategory($slug, $id = false)
    {
        $user = UserHelp::getUser(true);
        return $this->getData($id, 'category', $user, $slug);
    }

    private function getData($id, $what = '', $user = false, $slug = '')
    {
        $query =
            Story::limit(self::$chunkSize)->
            where('active', '=', 1)->
            orderBy('id', 'desc')->
            with(['user' => function ($q) {
                $q->select('id', 'nickname', 'nickname_slug', 'avatar');
            }])->
            with('category');


        if ($what == 'category') {

            $category = Category::where('slug', $slug)->get()->first();

            if ($category == null) {
                return Response::json(['errors' => ['unknown category']], 200);
            }
            $query->whereHas('category', function ($q) use ($category) {
                $q->where('category_id', $category->id);
            });

        }

        if ($user) {
            $query->with(['meta' => function ($q) use ($user) {
                $q->where('user_id', $user->id)->
                select('story_id', 'read', 'favorite');
            }]);
        }

        if (in_array($what, ['read', 'favorite'])) {
            $query->whereHas('meta', function ($q) use ($user, $what) {
                $q->where($what, '1')->
                where('user_id', $user->id);
            });
        } elseif ($user && $what != 'category') {
            $query->whereHas('meta', function ($q) use ($user) {
                $q->where('read', '1')->
                where('user_id', $user->id);
            }, '<', 1);
        }


        if (!$id) {
            $response = $query->get();
        } else {
            $response = $query->where('id', '<', $id)->get();
        }

        $flags = [
            'size' => self::$chunkSize
        ];

        $new = self::mapResponse($response, $user);

        return Response::json(['content' => $new, 'flags' => $flags], 200);
    }

    public static function mapResponse($response, $user)
    {
        return $response->map(function ($item) {

            $array = $item->toArray();

            $array['user']['avatar'] = UserHelp::smallAvatar($array['user']['avatar']);

            if ($array['active'] == 0) {
                $array['can_edit'] = true;
            } else {
                $array['can_edit'] = false;
            }

           // unset($array['active']);

            $meta = [
                'read' => 0,
                'favorite' => 0
            ];

            if (isset($array['meta'][0])) {
                $meta['read'] = $array['meta'][0]['read'];
                $meta['favorite'] = $array['meta'][0]['favorite'];
            }
            $array['meta'] = $meta;

            if (!empty($array['category'][0])) {
                unset($array['category'][0]['pivot']);
                $array['category'] = $array['category'][0];
            } else {
                $array['category'] = new stdClass();
            }


            return $array;

        });
    }


}
