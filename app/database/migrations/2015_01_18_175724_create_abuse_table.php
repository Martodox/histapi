<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbuseTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abuse', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->longText('reason');
            $table->integer('user_id')->unsigned();
            $table->foreign('story_id')->references('id')->on('stories');
            $table->integer('story_id')->unsigned();
            $table->boolean('resolved')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('abuse');
    }

}
