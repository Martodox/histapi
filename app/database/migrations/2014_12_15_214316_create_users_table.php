<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('facebook_id')->unique()->unsigned();
            $table->timestamps();
            $table->string('email');
            $table->string('first_name');
            $table->string('gender');
            $table->string('last_name');
            $table->string('link');
            $table->string('locale');
            $table->string('name');
            $table->integer('age_range_max');
            $table->integer('age_range_min');
            $table->string('ip', 15);
            $table->string('timezone');
            $table->string('nickname');
            $table->string('nickname_slug');
            $table->string('avatar');
            $table->boolean('load_comments')->default(0);
            $table->boolean('allow_notification')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
