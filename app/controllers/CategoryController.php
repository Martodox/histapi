<?php

class CategoryController extends BaseController
{

    public function all()
    {
        $categories = Category::all();
        return Response::json($categories, 200);
    }

}
