<?php


/**
 * UserHistory
 *
 * @property integer $user_id
 * @property string $token
 * @property string $platform
 * @property string $ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $user
 * @method static \Illuminate\Database\Query\Builder|\UserHistory whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\UserHistory whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\UserHistory wherePlatform($value)
 * @method static \Illuminate\Database\Query\Builder|\UserHistory whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\UserHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\UserHistory whereUpdatedAt($value)
 */
class UserHistory extends Eloquent
{

    protected $table = 'user_history';

    protected $fillable = [
        'user_id',
        'token',
        'ip'
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }


}
