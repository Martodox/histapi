<?php


/**
 * Comment
 *
 * @property integer $id
 * @property integer $story_id
 * @property integer $user_id
 * @property integer $parent_id
 * @property boolean $active
 * @property string $content
 * @property integer $up_votes
 * @property integer $down_votes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Story $story
 * @property-read \User $user
 * @method static \Illuminate\Database\Query\Builder|\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereStoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUpVotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereDownVotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUpdatedAt($value)
 */
class Comment extends Eloquent
{

    protected $hidden = [
        'up_votes',
        'down_votes'
    ];

    protected $fillable = [
        'story_id',
        'user_id',
        'parent_id',
        'active',
        'content'
    ];

    public function story()
    {
        return $this->belongsTo('Story');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }


}
