<?php

class TagController extends BaseController
{

    public function listTags($part)
    {
        $tags = Tag::where('tag', 'LIKE', '%' . $part . '%')->limit(20)->get();

        return Response::json($tags, 200);
    }

    public function all()
    {
        $tags = Tag::all();
        return Response::json($tags, 200);
    }

}
