<?php

class StorySeed extends Seeder
{

    private $numberOfEntries = 1000;

    public function run()
    {
        DB::table('stories')->delete();

        $tadeusz = 'Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię stracił. Dziś człowieka nie czytano w służbę rządu, by była żałoba, tylko się na Lombardzkiem polu. Jak ów mąż, bóg wojny otoczony chmurą pułków, tysiącem dział zbrojny wprzągłszy w pośrodku zamczyska którego widne były świeżo polewane. Tuż i oczy podniósł, i gestami ją piastował. Gdy się od powicia. Lecz wtenczas panowało takie oślepienie, Że nie ważą. Więc szlachcic młody panek i cztery ich nie mogę na kogoś czekało. Stryj nieraz dziad żebrzący chleba gałeczki trzy osoby na czterech ławach cztery ich nie powiedział choć świadka nie poruczy, bo tak na początek dać małą wieś, a drugą do łona a Pan świata wie, że polskie ubrani nagotowane z nich jedna ściana okna bez szyb, lecz stało wody pełne naczynie blaszane ale nigdzie nie puste, bo tak pan Hrabia z mosiężnymi dzwonki. Tam stała młoda dziewczyna. - mój Tadeuszu bo tak i nurkiem płynął na nim trzy z wolna krocz stado cielic tyrolskich z Kapitol i silni do Litwy kwestarz z wolna krocz stado cielic tyrolskich z wieczerzą powynosić z Kapitol i westchnień, i przyjaciel domu). Widząc gościa, na drzwi od przodków swoich nie bywa od starych zmienia jej nie chcą znać było, że się sprawa. My od rana wiedział, czy wstydzić, czy młodzież czekają. Pójdziemy, jeśli zechcesz, i ukazach licznyc sprawa wróciła znowu do tych imion spisem woźnemu jest pewna odmiana. Trzeba się uczyli. u wniścia alkowy i mami. Już nie powiedziała kogo owa szczęśliwa gałka oznaczała. Inaczej bawiono się od Moskali, skakał kryć się zabawiać gości nie znał polowania. On mnie dziecko do złotego run on ekwipaż parskali ze skoszonej łąki. Wszystko bieży ku studni. najlepiej z odmienną modą, pod bramę. We dworze jak od ciemnej zieleni topoli, co o porządku, nikt tam nie dostrzegł, nazbyt rychło znikła ale szerzej niż się możemy na tem, Że w oszmiańskim powiecie przyjechał z rana, bo tak się stempel na Francuza, Że nie wąchał pieniędzy i przeplatane różowymi wstęgi pośród nich jedna ściana okna bez przerwy rzecz długa, choć młodzież czekają. Pójdziemy, jeśli zechcesz, i gdzie usłyszał głos zabierać. Umilkli wszyscy poszli za nim na drzwi od wiatrów jesieni. Dóm mieszkalny niewielki, lecz lekki. odgadniesz, że tak myślili starzy. A na skarb carski zabierano. Czasem do kraju. Mowy starca krążyły we dworze jak zdrowie. Ile cię trzeba cenić, ten Bonapart figurka! Bez Suworowa to mówiąc, że odbite od obywateli. I Tadeusz Telimenie, lecz go tylko cichy smutek panów lub cicha i obrok, i niezgrabny. Zatem się do nowej mody odsyłać konie rżące lecą ze żniwa i każdego wodza legijonu i z dokumentów przekonywał o Bonaparta, zawsze służy której nigdy na Tadeusza, widać, że posiadłość tam pogląda, gdzie śród biesiadników siedział słuchał rozmowy grzeczne z łąk, i mniej piękne, niż obcej mody odmianą się z harbajtelem zawiązanym w końcu dzieje domowe powiatu dawano przez to mówiąc, że gotyckiej są rowni. Choć Sędzia wie, że były czary przeciw czarów. Raz w pół kroku Tak każe u niego nie miał przyjść wkrótce wielki post - tak piękny chart z miasta, ze zdań wyciągała na szabli, a drugą do lasu bawić się Soplica. wszyscy poszli za nim. Sława czynów tylu szlachty, w wieku mu odwiązał, pas mu poważnie rękę dał mu poważnie rękę do Ojczyzn pierwszy Podkomorzy i ręce pod bramę. We dworze jak po kądzieli a oni tak rzuciły. Tuż stało wody pełne naczynie blaszane ale nigdzie nie wierzono rzeczom najdawniejszym w jasełkach ukryte chłopięta. Biegła i narody słyną z flinty strzelać albo bierze. Nawet tak były rączki, co w tylu lat siedzi Rejtan żałośny po kądzieli a zwierzę nie dostrzegł, nazbyt rychło znikła ale powiedzieć nie w pośrodku zamczyska którego niespodzianie spada grom po imieniu. Herb Horeszków, Półkozic, jaśniał na spoczynek powraca. Już krąg promienisty spuszcza się tłocz i zalety Ściągnęły wzrok stryja ku studni, której już robił projekt, że przychodził już nie wąchał pieniędzy i stoi wypisany każdy po kryjomu. Chłopiec, co jasnej bronisz Częstochowy i Hrabia ma żądło w słów kilka wyrzekł, do nowej mody małpowanie, milczał. boby krzyczała młodzież, że słuchał rozmowy grzeczne z Bonapartą. tu Ryków przerwał i ziemianinowi ustępować z wolna gładząc faworyty rzekł wojewoda Niesiołowski stary stojący zegar kurantowy w kraty. Pas taki można wydrukować wszystkie procesu wypadki spotkanie się, by stary który ma sto wozów sieci w szkole. Ale co wzdłuż i zalety Ściągnęły wzrok stryja ku studni, której nigdy nie ustawiał a między wierzycieli. Zamku żaden wziąść nie na filarach, podłoga wysłana kamieniem, Ściany bez szyb, lecz nim stał kwestarz, Sędzia gości siedział. Pan świata wie, jak znawcy, ci wesele. Jest z jego zasięgała zdania innych. więc wszyscy i od płaczu! On opowiadał, jako w które wylotem kontusz otarł prędko, jak od ganku zamknięty zaszczepkami i wszystkich w ciąg powieści, pytań wykrzykników i dwiestu strzelców licznych i opisuję, bo tak Suwarów w bliskiej wiosce na tyle nauki lękał się, toczył zdumione źrenic po duszy, a chłopi żegnali się, że jacyś Francuzi wymowny zrobili wynalazek: iż ludzie są architektury. Choć Sędzia z łowów wracając trafia się, jak naoczne świadki. I zląkł się, że miał czasu. Na piasku drobnym, suchym, białym w szklankę panny córki choć najwymowniejsza. Ale stryj nie dozwalał, by stary i tylko są rowni. Choć pani ta chwała należy urządzając we brzozowym gaju stał kwestarz, Sędzia jego bok usiadła owa szczęśliwa gałka oznaczała. Inaczej bawiono się nieco wylotów kontusza nalał węgrzyna i Asesor, razem, jakoby zlewa. I bór czernił się do marszu! Pójdziem, czy go przez wzgląd męża dla skończenia dawnego z dziecinną radością pociągnął za rarogiem zazdroszczono domowi, przed trybunałem. Jedna ręka na francuskim wózku pierwszy Podkomorzy i przymioty. Stąd droga co je wicher rozerwie i kołkiem zaszczepki przetknięto. Podróżny do zamczyska? Nikt go rodzi? Z kim był, opisywać długo. Dosyć, że spod ramion wytknął palce i świadki. I Tadeusz przyglądał się nieznanej osobie przypomniał, że za stołem. Z wieku mu jego puchar i wstąg jasnych pęki. Ta przerwa rozmów trwała już byli z rana, bo tak rzuciły. Tuż i stąd się o tem nic to mówiąc, że przychodził już zjechał z nim trzy z Soplicą:.';

        $categories = Category::all();

        for ($i = 0; $i < $this->numberOfEntries; $i++) {
            $length = rand(1, 15) * 50;
            $rand = rand(0, strlen($tadeusz) - $length);
            $text = substr($tadeusz, $rand, $length);

            $titleLength = rand(1, 10) * 10;
            $rand = rand(0, strlen($tadeusz) - $titleLength);
            $title = substr($tadeusz, $rand, $titleLength);

            $story = new Story();

            $category = $categories->random();

            $story->content = $text;
            $story->active = 1;
            $story->user_id = '1';
            $story->language = 'pl';
            $story->title = mb_strtoupper($title);
            $story->save();
            $story->category()->attach([$category->id]);

            $this->command->info('Entry ' . ($i + 1) . ' from ' . $this->numberOfEntries);
        }
        $this->command->info('Stories table seeded!');
    }

}