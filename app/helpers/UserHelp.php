<?php

class UserHelp
{
    public static function getUser($gracefully = false)
    {
        //TODO: join two queries into one and with tokens too!

        if ($gracefully) {
            return UserHelp::getUserNoWarn();
        } else {
            return UserHelp::getUserWarn();
        }
    }

    private static function getUserNoWarn()
    {
        $authHeader = Request::header('authorization');

        if (empty($authHeader)) {
            return false;
        }

        $user = CustomSession::where('token', $authHeader)->get()->first();
        if (empty($user)) {
            return false;
        } else {
            $user =
                User::where('facebook_id', $user->id)->
                get()->
                first();

            if (empty($user)) {
                return false;
            } else {
                return $user;
            }

        }
    }

    private static function getUserWarn()
    {
        $authHeader = Request::header('authorization');

        if (empty($authHeader)) {
            App::abort(401, 'Unauthorized action.');
        }

        $user = CustomSession::where('token', $authHeader)->get()->first();


        if (empty($user)) {
            App::abort(401, 'Unauthorized action.');
        } else {
            return User::where('facebook_id', $user->id)->get()->first();
        }
    }

    public static function moderator()
    {

        $moderatorsArray = ['319173321621367', '751752791528961'];

        $user = self::getUserWarn();

        if (!in_array($user->facebook_id, $moderatorsArray)) {
            App::abort(401, 'Unauthorized action.');
        }

        return $user;


    }

    public static function smallAvatar($url)
    {
        return substr($url, 0, strlen($url) - 4) . '_s.jpg';
    }

}