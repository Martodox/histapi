<?php

/**
 * Tag
 *
 * @property integer $id
 * @property string $tag
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\Story[] $posts
 * @method static \Illuminate\Database\Query\Builder|\Tag whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereTag($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereSlug($value)
 */
class Tag extends Eloquent
{
    protected $table = 'tags';
    public $timestamps = false;

    protected $fillable = [
        'tag',
        'slug'
    ];

    public function posts()
    {
        return $this->belongsToMany('Story')->withPivot('tag_id', 'story_id');
    }
}
