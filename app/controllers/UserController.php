<?php

class UserController extends BaseController
{


    public static function getUser()
    {
        $authHeader = Request::header('authorization');

        $user = CustomSession::where('token', $authHeader)->first()->get();
        if ($user->isEmpty()) {
            return Response::make('not authorised', 401);
        }
        $user = $user->toArray();

        return User::find($user[0]['id']);
    }


    public function updateProfile()
    {
        $user = UserHelp::getUser();
        $avatar = Sanitize::get('avatar');
        $nickname = Sanitize::get('nickname');
        $autoload = Sanitize::get('autoload');
        $allowNotification = Sanitize::get('allowNotification');

        $response['userUpdate'] = [];

        if (!empty($avatar) || !empty($nickname) || is_numeric($autoload) || is_numeric($allowNotification)) {
            $update = User::find($user->id);
            $doCheck = true;
        } else {
            $doCheck = false;
        }

        if (!$doCheck) {
            $response['userUpdate']['updated'] = $doCheck;
            return Response::json($response, 200);
        }

        if (!empty($avatar)) {
            try {
                $hash = md5($avatar);
                $img = Image::make(base64_decode($avatar));

                $img->fit(200)->encode('jpg', 75);
                $img->save('avatars/' . $hash . '.jpg');

                $img = Image::make(base64_decode($avatar));


                $img->fit(70)->encode('jpg', 75);
                $img->save('avatars/' . $hash . '_s.jpg');

                $link = Request::root() . '/avatars/' . $hash . '.jpg';
                $update->avatar = $link;
                $response['userUpdate']['avatar'] = $link;
            } catch (Intervention\Image\Exception\NotReadableException $e) {
                $response['userUpdate']['avatar'] = false;
                $response['userUpdate']['avatar']['errors'] = ['bad image'];

            }


        }

        if (!empty($nickname)) {

            $nickname_slug = Str::slug($nickname);

            $isUnique = User::where('nickname', $nickname)->get();
            $isSlugUnique = User::where('nickname_slug', $nickname_slug)->get();

            if (count($isUnique) > 0 || count($isSlugUnique) > 0) {
                $response['userUpdate']['nickname'] = false;
                $response['userUpdate']['nickname']['errors'] = ['nickname already taken'];
            } else {
                $response['userUpdate']['nickname'] = $nickname;
                $update->nickname = $nickname;
                $update->nickname_slug = $nickname_slug;
            }
        }

        if (is_numeric($autoload)) {
            $update->load_comments = $autoload;
            $response['userUpdate']['autoload'] = $autoload;
        }

        if (is_numeric($allowNotification)) {
            $update->allow_notification = $allowNotification;
            $response['userUpdate']['allowNotification'] = $allowNotification;
        }

        if ($doCheck) {
            $update->save();
        }


        return Response::json($response, 200);


    }

    public function getProfile()
    {
        $user = UserHelp::getUser();
        return Response::json(['user' => $user], 200);
    }

}
